FROM alpine:latest

ARG GITLAB_BUILD_ID

RUN addgroup -g 82 -S www-data && \
    adduser -u 82 -D -S -G www-data www-data

RUN apk add --update \
    wget \
    unzip \
    ca-certificates

RUN mkdir -p /var/www && \
    cd /var/www && \
    wget -q https://gitlab.com/ttanttakun/ttanttakun.org/builds/${GITLAB_BUILD_ID}/artifacts/download -O release.zip && \
    unzip -qq release.zip && \
    rm release.zip && \
    chown -R 82:82 /var/www && \
    apk del --purge wget unzip

WORKDIR /var/www
VOLUME /var/www

COPY docker-entrypoint.sh /usr/local/bin/
CMD ["docker-entrypoint.sh"]
