#!/bin/sh

cd /var/www

echo DB_NAME=\"$DB_NAME\" >> .env
echo DB_USER=\"$DB_USER\" >> .env
echo DB_PASSWORD=\"$DB_PASSWORD\" >> .env
echo DB_HOST=\"$DB_HOST\" >> .env

echo WP_ENV=\"$WP_ENV\" >> .env
echo WP_HOME=\"$WP_HOME\" >> .env
echo WP_SITEURL=\"$WP_HOME/wp\" >> .env

# Generate your keys here: https://api.wordpress.org/secret-key/1.1/salt/
echo AUTH_KEY=\"$AUTH_KEY\" >> .env
echo SECURE_AUTH_KEY=\"$SECURE_AUTH_KEY\" >> .env
echo LOGGED_IN_KEY=\"$LOGGED_IN_KEY\" >> .env
echo NONCE_KEY=\"$NONCE_KEY\" >> .env
echo AUTH_SALT=\"$AUTH_SALT\" >> .env
echo SECURE_AUTH_SALT=\"$SECURE_AUTH_SALT\" >> .env
echo LOGGED_IN_SALT=\"$LOGGED_IN_SALT\" >> .env
echo NONCE_SALT=\"$NONCE_SALT\" >> .env

chown 82:82 .env

exec /bin/ash
